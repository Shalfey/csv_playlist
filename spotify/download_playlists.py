#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 20 00:37:45 2018

@author: rageeb
"""

# Importing modules
import requests
import csv
import os, errno


# Taking the url as input to extract token
input_data =input('Please Enter the url you redirect to after authorization. Pattern: "https://localhost/#token...": ')
print()

# Extracting token from the url and storing as variable
token = input_data.split('&')[0].replace('https://localhost/#access_token=','')

 

# Declaring python lists to store Playlist names and urls
playlist_names = []
playlist_urls = []


#Making the header for authorization
payload = {'Authorization': 'Bearer '+ token}

# Doing request with Authorization
playlist_json = requests.get('https://api.spotify.com/v1/me/playlists', headers=payload)
# Converting return object into JSON
playlist_json = playlist_json.json()

# Extracting Playlist names and urls and storing them into list
for playlist in playlist_json['items']:
    playlist_names.append(playlist['name'])
    playlist_urls.append(playlist['tracks']['href'])


while playlist_json['next']:
    playlist_json = requests.get(str(playlist_json['next']), headers=payload)
    
    for playlist in playlist_json['items']:
        playlist_names.append(playlist['name'])
        playlist_urls.append(playlist['tracks']['href'])





# Starting looping through playlist urls
for index, url in enumerate(playlist_urls):
    
    # Declaring lists to store artist and track names
    artist_names = []
    track_names = []
    
    
    # Making the request with authoriztion
    tracks_json = requests.get(url, headers=payload)
    
    # Converting return object into JSON
    tracks_json = tracks_json.json()
    
    # Extracting artist and track names and storing into list
    for item in tracks_json['items']:
        track = item['track']
        artist_names.append(track['artists'][0]['name'])
        track_names.append(track['name'])
      
    
    # Handling pagination for tracks
    while tracks_json['next']:
        tracks_json = requests.get(str(tracks_json['next']), headers=payload)
        tracks_json = tracks_json.json()
        
        for item in tracks_json['items']:
            track = item['track']
            artist_names.append(track['artists'][0]['name'])
            track_names.append(track['name'])
    
    # Setting CSV file name as playlist
    filename = playlist_names[index] + '.csv'
    

    
    # Exporting data into CSV file
    with open(filename, 'w', newline='') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for i in range(len(artist_names)):
            spamwriter.writerow([track_names[i], artist_names[i]])
            
    print(filename + ' is exported')
    
print()    
print('Done')
