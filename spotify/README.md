Before using the script you need to give it access to your playlists. This is done by visiting the link below and following the instructions, once done you will be redirected to a (hopefully) non-existing localhost URI which you must copy and supply to the script when it asks for it.

[https://accounts.spotify.com/authorize?client_id=dcef883b41d6483da8096f7a2215e1dd&redirect_uri=https:%2F%2Flocalhost&scope=playlist-read-private playlist-read-collaborative&response_type=token&state=123](https://accounts.spotify.com/authorize?client_id=dcef883b41d6483da8096f7a2215e1dd&redirect_uri=https:%2F%2Flocalhost&scope=playlist-read-private playlist-read-collaborative&response_type=token&state=123)

The redirect URL should look something like this.

```
https://localhost/#access_token=BQDMsj4l...IB7dHUsbpUo&token_type=Bearer&expires_in=3600&state=123
```

**Because this script's Spotify client ID is publicly available you are in fact giving anyone with some computer skills read access to you playlists!**

To minimize this problem I suggest you remove the script's access to your account immediately after downloading the playlists. This can be done at the following link. The app is called "CSV Playlist".

[https://www.spotify.com/account/apps/](https://www.spotify.com/account/apps/)

If you are even more paranoid you could create your own Spotify app with a client ID only you know and use that one.

1. Go to [https://developer.spotify.com/dashboard](https://developer.spotify.com/dashboard)
2. Click "create a client id" and follow the instructions. Make sure you add "https://localhost" as a redirect URI for the app.
3. Replace the client ID in the authorization link with your client id and follow the normal instructions.
